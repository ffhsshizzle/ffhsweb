<?php

function top() {

	?>
	<!doctype html>
	<html>
		<head>
			<meta charset="utf-8" />
			<link href="common.css" rel="stylesheet" type="text/css" />
			<link href="tipuedrop/tipuedrop.css" rel="stylesheet" type="text/css" />
		</head>
		<body>
			<nav>
				<h1>Menu</h1>
				<ul>
					<li><a href="customers.php">Kunden</a></li>
					<li><a href="products.php">Produkte</a></li>
					<li><a href="orders.php">Bestellungen</a></li>
					<li><a href="deliveries.php">Lieferungen</a></li>
					<li><a href="bills.php">Rechnungen</a></li>
				</ul>
			</nav>
			<div id="Contents">
				<?php

			}

			function bottom() {

				?>
			</div>
			<script src="jquery.js"></script>
			<!-- below line must be set for each page individually! -->
			<!--script src="tipuedrop/tipuedrop_content.js"></script-->
			<script src="tipuedrop/tipuedrop.min.js"></script>
			<script>
				$( document ).ready( function() {
					$( '#tipue_drop_input' ).tipuedrop();
				} );
			</script>
			<script src="common.js"></script>
		</body>
	</html>
	<?php

}
