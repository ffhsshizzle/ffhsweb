<?php
require "_framework.php";
require "_mockupData.php";

top();

//$name = "";
$id = "";

if( !empty( $_GET["data"] ) ) {
	$name = $_GET["data"];
}

$orders = array("B1234", "B4321");

$lieferungenPendent = array(
	"L2584",
	"L2687"
);

$lieferungenErhalten = array(
	"L1224",
	"L1957"
);

?>
<div id="Search">
<?php
if( isset( $name ) ) {
	echo '<h1>Kunden - ' . $name;
} else {
	echo '<h1>Kunden – Alfons Müller</h1>';
}
?>
	
	<div id="Searchbar__Wrapper">
		<input type="text" id="tipue_drop_input" autocomplete="off" />
		<div id="tipue_drop_content"></div>
		<span id="Searchbar__Pictogram"></span>
	</div>
	<div id="Details">
		<h2>Details</h2>
		<i>Details zum Kunden</i>
	</div>

<?php

echo '</div>';

echo '<div id="Overlay">';
echo '<div id="Overlay__Menu">';

echo '<h2>Offene Bestellungen</h2>';
if( count( $orders ) > 0 ) {
	echo '<ul>';
	foreach( $orders as $order ) {
		echo '<li><a href="order.php?data=' . $order . '">Bestellung #' . $order . '</a></li>';
	}
	echo '</ul>';
} else {
	echo '<p>Keine Bestellungen</p>';
}

echo '<h2>Lieferungen pendent</h2>';
if( count( $lieferungenPendent ) > 0 ) {
	echo '<ul>';
	foreach( $lieferungenPendent as $d ) {
		echo '<li><a href="delivery.php?data=' . $d . '">Lieferung #' . $d . '</a></li>';
	}
	echo '</ul>';
} else {
	echo '<p>Keine Lieferungen</p>';
}

echo '<h2>Lieferungen erhalten</h2>';
if( count( $lieferungenErhalten ) > 0 ) {
	echo '<ul>';
	foreach( $lieferungenErhalten as $d ) {
		echo '<li><a href="delivery.php?data=' . $d . '">Lieferung #' . $d . '</a></li>';
	}
	echo '</ul>';
} else {
	echo '<p>Keine Lieferungen</p>';
}

echo '<div>';
echo '</div>';
echo '<script src="tipuedrop/customers.js"></script>';

bottom();
