<?php

require "_framework.php";

$history = array(
	'B1234',
	'B2468',
	'B4321',
);

top();

?>
<div id="Search">
	<h1>Bestellungen – #4321</h1>
	<div id="Searchbar__Wrapper">
		<input type="text" id="tipue_drop_input" autocomplete="off" />
		<div id="tipue_drop_content"></div>
		<span id="Searchbar__Pictogram"></span>
	</div>
	<div id="Search__History">
		<h2 class="Search__History--title">History</h2>
		<ul>
<?php

foreach( $history as $item ) {
	printf( '<li><a href="order.php?data=%s">#%s</a></li>', $item, $item );
}

?>
		</ul>
	</div>
</div>
<script src="tipuedrop/orders.js"></script>
<?php

bottom();
