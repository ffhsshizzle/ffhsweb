jQuery.fn.menuE = function( settings ) {
	var tabKeyPressed = false;

	jQuery.extend( {
		overlay: null
	}, settings );

	var overlay = settings.overlay;

	if( null === settings.overlay ) {
		throw "You must provide an object with property 'overlay' as jQuery object of the overlay that contains the menu.";
	}
	if( 0 === settings.overlay.length ) {
		throw "You must provide a jQuery object with matching elements."
	}

	function menuToggle( e ) {
		if( e.shiftKey || e.keyCode !== 9 ) {
			return;
		}

		e.preventDefault();

		overlay.toggleClass( 'show' );
	}

	this.keypress( menuToggle );
}

$( document ).ready( function() {
	$( document.body ).menuE( {
		overlay: $( '#Overlay' )
	} );
} );
