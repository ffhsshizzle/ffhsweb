<?php

require "_framework.php";

$history = array(
	'P6374',
	'P4227',
	'P5477',
);

top();

?>
<div id="Search">
	<h1>Produkte</h1>
	<div id="Searchbar__Wrapper">
		<input type="text" id="tipue_drop_input" autocomplete="off" />
		<div id="tipue_drop_content"></div>
		<span id="Searchbar__Pictogram"></span>
	</div>
	<div id="Search__History">
		<h2 class="Search__History--title">History</h2>
		<ul>
<?php

foreach( $history as $item ) {
	printf( '<li><a href="product.php?data=%s">#%s</a></li>', $item, $item );
}

?>
		</ul>
	</div>
</div>
<script src="tipuedrop/products.js"></script>
<?php

bottom();
