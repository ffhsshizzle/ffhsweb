<?php

require "_framework.php";

if( !empty( $_GET['data'] ) ) {
	$data = $_GET['data'];
}

$kunden = array(
	"Alfons Müller",
);

$orders = array("B1234", "B4321");

top();

echo '<div id="Search">';

if( isset( $data ) ) {
	echo '<h1>Lieferungen - #' . $data;
} else {
	echo '<h1>Lieferungen – #2134</h1>';
}
?>

	<div id="Searchbar__Wrapper">
		<input type="text" id="tipue_drop_input" autocomplete="off" />
		<div id="tipue_drop_content"></div>
		<span id="Searchbar__Pictogram"></span>
	</div>
	<div id="Search__History">
		<h2 class="Search__History--title">Details</h2>
		<i>Details zur Lieferung</i>
	</div>

<?php

echo '</div>';

echo '<div id="Overlay">';
echo '<div id="Overlay__Menu">';

echo '<h2>Kunde</h2>';
if( count( $kunden ) > 0 ) {
	echo '<ul>';
	foreach( $kunden as $d ) {
		echo '<li><a href="customer.php?data=' . $d . '">' . $d . '</a></li>';
	}
	echo '</ul>';
} else {
	echo '<p>Keine Kunden</p>';
}

echo '<h2>Offene Bestellungen</h2>';
if( count( $orders ) > 0 ) {
	echo '<ul>';
	foreach( $orders as $d ) {
		echo '<li><a href="order.php?data=' . $d . '">Bestellung #' . $d . '</a></li>';
	}
	echo '</ul>';
} else {
	echo '<p>Keine Bestellungen</p>';
}
echo '<div>';
echo '</div>';
echo '<script src="tipuedrop/deliveries.js"></script>';

bottom();
