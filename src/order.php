<?php

require "_framework.php";
//require "_mockupData.php";

if( !empty( $_GET['data'] ) ) {
	$data = $_GET['data'];
}

$kunden = array(
	"Alfons Müller",
);

$lieferungenPendent = array(
	"L2584",
	"L2687"
);

$lieferungenErhalten = array(
	"L1224",
	"L1957"
);

top();

echo '<div id="Search">';

if( isset( $data ) ) {
	echo '<h1>Bestellungen - #' . $data;
} else {
	echo '<h1>Bestellungen – #4321</h1>';
}
?>

	<div id="Searchbar__Wrapper">
		<input type="text" id="tipue_drop_input" autocomplete="off" />
		<div id="tipue_drop_content"></div>
		<span id="Searchbar__Pictogram"></span>
	</div>
	<div id="Search__History">
		<h2 class="Search__History--title">Details</h2>
		<i>Details zur Bestellung</i>
	</div>

<?php

echo '</div>';

echo '<div id="Overlay">';
echo '<div id="Overlay__Menu">';

echo '<h2>Kunde</h2>';
if( count( $kunden ) > 0 ) {
	echo '<ul>';
	foreach( $kunden as $d ) {
		echo '<li><a href="customer.php?data=' . $d . '">' . $d . '</a></li>';
	}
	echo '</ul>';
} else {
	echo '<p>Keine Kunden</p>';
}

echo '<h2>Lieferungen pendent</h2>';
if( count( $lieferungenPendent ) > 0 ) {
	echo '<ul>';
	foreach( $lieferungenPendent as $d ) {
		echo '<li><a href="delivery.php?data=' . $d . '">Lieferung #' . $d . '</a></li>';
	}
	echo '</ul>';
} else {
	echo '<p>Keine Lieferungen</p>';
}

echo '<h2>Lieferungen erhalten</h2>';
if( count( $lieferungenErhalten ) > 0 ) {
	echo '<ul>';
	foreach( $lieferungenErhalten as $d ) {
		echo '<li><a href="delivery.php?data=' . $d . '">Lieferung #' . $d . '</a></li>';
	}
	echo '</ul>';
} else {
	echo '<p>Keine Lieferungen</p>';
}

echo '<div>';
echo '</div>';
echo '<script src="tipuedrop/orders.js"></script>';

bottom();
